<?php while (have_posts()) : the_post(); ?>
	
	<?php
		// get welcome images
		$acf_images = get_field('welcome_images');
		$i = 0;
		while( $i < 3 ){
			if( is_array ( $acf_images[$i] ) ){
				$images[$i] = $acf_images[$i];
			}
			else{
				$images[$i]['welcome_image'] = "";
				$images[$i]['welcome_filter_opacity'] = "";
				$images[$i]['welcome_filter_color'] = "";
			}
			$i++;
		}
	?>
	<div class="container">
		<div class="row no-gutter">
			<div class="col-sm-12 col-lg-6 no-gutter" id="main-col">
				<div class="col-lg-12 bg-yellow no-gutter">
					<div class="overlay-wrapper" id="welcome-one-ow">
						<div class="overlay-bg" id="welcome-one-obg" style="background-image: url('<?php echo $images[0]['welcome_image']; ?>');"></div>
						<div class='overlay' id="welcome-one-o" style="background: rgba(<?php echo 	$rgba = implode ( ',' , getrgb( $images[0]['welcome_filter_color'])); ?> , <?php echo $images[0]['welcome_filter_opacity']/100; ?>);"></div>
						<div class='overlay-content' id="welcome-one-oc">
		            		<h2><?php echo get_field('welcome_title'); ?></h2>
							<div class='vert-center'>
								<div class="welcome-one-content">
				            		<?php the_content(); ?>
									<div class='welcome-one-secondary'>
					            		<?php echo get_field('welcome_secondary_text'); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="hidden-md hidden-sm hidden-xs col-lg-6 no-gutter bg-yellow">
					<?php get_template_part( 'templates/first-image-filter/filter' , 'div'); ?>
				</div>
				<div class="hidden-md hidden-sm hidden-xs col-lg-6 no-gutter bg-yellow">
					<div class="overlay-wrapper" id="welcome-three-ow">
						<?php get_template_part( 'templates/second-image-filter/filter' , 'div'); ?>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-3">
				<?php get_template_part( 'templates/vendor-image-filter/filter' , 'div'); ?>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-3">
				<?php get_template_part( 'templates/events-image-filter/filter' , 'div'); ?>
			</div>
		</div>
	</div><!--/.container-->	
	<script>
		// get the height of the screen, subtract the navbar height, set the banner to the difference.
		function setWelcome(){
			var	footerHeight = 62,
				mb = document.getElementById('main-col'),
				headerHeight = document.getElementById('themeslug-navbar').clientHeight,
				viewX = Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
				viewY = Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
				newHeight = ( viewY - headerHeight );
				if( viewX > 768 ){
					newHeight = newHeight - footerHeight;
					if( newHeight < 700 ){ newHeight = 700; }
				}
				else{
					newHeight = 600;
				}
				// use viewX for responsive if you want: 	
					mb.style.height = newHeight + 'px'; 
		}
		setWelcome();
	</script>
<?php endwhile; ?>
