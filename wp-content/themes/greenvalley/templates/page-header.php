<?php
	function title() {
	  if (is_home()) {
	    if (get_option('page_for_posts', true)) {
	      echo get_the_title(get_option('page_for_posts', true));
	    } else {
	      echo __('Latest Posts', 'dorado');
	    }
	  } elseif (is_post_type_archive('event')) {
		echo "Events";		
	  } elseif (is_search()) {
	    echo sprintf(__('Search Results for %s', 'dorado'), get_search_query());
	  } elseif (is_404()) {
	    echo __('Not Found', 'dorado');
	  } else {
	    echo get_the_title();
	  }
	}
?>
<div class="page-header">
  <h1><?php title(); ?></h1>
</div>
