<?php
// search and replace "events" with unique id

add_action('wp_enqueue_scripts', 'events_load_filter_assets', 100);
function events_load_filter_assets(){
	wp_enqueue_style('events_filter_style', get_template_directory_uri(). '/templates/events-image-filter/filter-styles.css', false, null);
	wp_enqueue_script('events_filter_script', get_template_directory_uri(). '/templates/events-image-filter/filter-scripts.js', array(), null, true);
}

if( function_exists('acf_add_local_field_group') ){
	$home_post = get_page_by_path('Home');
	$home = 0;
	if( is_object( $home_post ) ){
		$home = $home_post->ID;
	}
	acf_add_local_field_group(
		array (
			'key' => 'events_filter_group',
			'title' => 'Events Block',
			'fields' => array (
				array (
					'key' => 'events_container_image',
					'label' => 'Background Image',
					'instructions' => 'Size should be x by x',
					'name' => 'events_container_image',
					'type' => 'image',
					'return_format' => 'array'
				),
				array (
					'key' => 'events_container_color',
					'label' => 'Color',
					'instructions' => 'Set color of background image overlay',
					'name' => 'events_container_color',
					'type' => 'color_picker' 
				),
				array (
					'key' => 'events_container_opacity',
					'label' => 'Opacity',
					'instructions' => 'Set opacity of background image overlay (from 0 to 100 , where 0 is transparent)',
					'name' => 'events_container_opacity',
					'type' => 'number' 
				),
/*				array (
					'key' => 'events_container_content',
					'label' => 'Static State Content',
					'name' => 'events_container_content',
					'type' => 'text' // or use text or textarea
				),
				array (
					'key' => 'events_container_hover_content',
					'label' => 'Hover State Content',
					'name' => 'events_container_hover_content',
					'type' => 'wysiwyg' // or use text or textarea
				),*/
			),
			'location' => array (
				array(
					array (
						'param' => 'page',
						'operator' => '==',
						'value' => $home,
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
		)
	);
}
