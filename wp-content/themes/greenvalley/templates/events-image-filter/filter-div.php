<?php 
	$imageArray = get_field('events_container_image');
	$overlayColor = get_field('events_container_color');
	$rgba = implode ( ',' , getrgb( $overlayColor) );
?>
	<?php if(is_array( $imageArray ) ) : ?>
		<div class="overlay-wrapper" id="events-ow">
			<div class="overlay-bg" id="events-obg" style="background-image: url('<?php echo $imageArray['url']; ?>');"></div>
			<div class='overlay' id="events-o" style="background: rgba(<?php echo $rgba; ?> , <?php echo get_field('events_container_opacity')/100; ?>);"></div>
			<div class='overlay-content' id="events-oc">
				<h2>Events</h2>
				<div class="events-content">

					<?php
			$event_obj = new doradoEvents;
			$start = date( 'Ymd');
			$event_obj->get_events($start,'' ,'',3);
			$events = $event_obj->events;
//						$events_query = new WP_Query($args);
						if( count( $events )> 0) : ?>
							<div class="event-slider">		
								<?php  foreach ($events as $daystamp): 
										foreach( $daystamp as $event ) :
										$event_id = $event['post'];
										$permalink = get_permalink( $event_id );
										$event_post = get_post($event_id);
//print_r($event_post);
?>
									<div>
										<div class="slide-content">
											<h3>
												<a href="<?php echo $permalink; ?>" rel="bookmark" ><?php echo $event_post->post_title; ?></a>
											</h3>
											<?php if( have_rows('dorado-events' , $event_id) ): $i = 1; ?>
												<div class='date-wrapper'>
													<ul class='date-list'>
											
													<?php while ( have_rows('dorado-events', $event_id)  ) : the_row();	
															if( $i <= 5 ){
														        // display a sub field value
														        $date = strtotime( get_sub_field('dorado_event_date') ); // convert date to unix; output by acf as Ymd: 20150618
																if( $date >= strtotime( $start ) ){
	
																	if ( $date ){
																		$date = date( 'F j, Y' , $date);
																	}
															        $time = get_sub_field('dorado_event_time'); ?>
														
																	<li><?php echo $date; ?><?php if( $time ){ echo " at ". $time; } ?></li>									
															<?php }} ?>													
												    <?php $i++; endwhile; ?>	
											
													</ul>
												</div>
											<?php endif; ?>					

											<div class="event-content">
												<?php echo $event_post->post_content; ?>
											</div>
										</div>
									</div>
								<?php endforeach; endforeach; ?>
							</div>
					<?php endif;  ?>
					<div class="button-wrap">
						<a href='/events' class="gv-button">View Events</a>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<script>
		// get the height of the screen, subtract the navbar height, set the banner to the difference.
		function setEvents(){
			var	footerHeight = 62,
				mb = document.getElementById('events-ow'),
				headerHeight = document.getElementById('themeslug-navbar').clientHeight,
				mb_bg = document.getElementById('events-obg'),
				mb_over = document.getElementById('events-o'),
				mb_content = document.getElementById('events-oc'),
				viewX = Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
				viewY = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
				newHeight = ( viewY );
				if( viewX > 768 ){
					newHeight = newHeight - headerHeight - footerHeight;
					if( newHeight < 700 ){ newHeight = 700; }
				}
				else{
					newHeight = 650;
				}
				// use viewX for responsive if you want: 	if( $viewX < 768 ){}
				mb.style.height = mb_bg.style.height = mb_over.style.height = mb_content.style.height = ( newHeight ) + 'px'; 
		}
		setEvents();
	</script>