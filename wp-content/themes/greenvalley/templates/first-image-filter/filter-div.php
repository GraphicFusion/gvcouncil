<?php 
	global $images;
	$rgba = implode ( ',' , getrgb( $images[1]['welcome_filter_color']));
?>
	<?php if(is_array( $images ) ) : ?>
		<div class="overlay-wrapper" id="welcome-two-ow">
			<div class="overlay-bg" id="welcome-two-obg" style="background-image: url('<?php echo $images[1]['welcome_image']; ?>');"></div>
			<div class='overlay' id="welcome-two-o" style="background: rgba(<?php echo $rgba; ?> , <php echo $images[1]['welcome_filter_opacity']/100; ?>);"></div>
			<div class='overlay-content' id="welcome-two-oc">
				<div class='vert-center'>
					<div class="welcome-two-content welcome-one-secondary">
			            <?php echo get_field('first_container_content'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
