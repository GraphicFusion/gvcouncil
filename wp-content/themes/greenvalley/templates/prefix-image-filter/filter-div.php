<?php 
	$imageArray = get_field('prefix_container_image');
	$overlayColor = get_field('prefix_container_color');
	$rgba = implode ( ',' , getrgb( $overlayColor) );
?>
	<?php if(is_array( $imageArray ) ) : ?>
		<div class="overlay-wrapper" id="prefix-ow">
			<div class="overlay-bg" id="prefix-obg" style="background-image: url('<?php echo $imageArray['url']; ?>');"></div>
			<div class='overlay' id="prefix-o" style="background: rgba(<?php echo $rgba; ?> , <?php echo get_field('prefix_container_opacity')/100; ?>);"></div>
			<div class='overlay-content' id="prefix-oc">
				<div class='vert-center'>
					<div class="prefix-content">
			            <?php echo get_field('prefix_container_content'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<script>
//comment out last line of script to manually set the image height (here it is set to the screen height)
		// get the height of the screen, subtract the navbar height, set the banner to the difference.
		var	mb = document.getElementById('prefix-ow'),
			mb_bg = document.getElementById('prefix-obg'),
			mb_over = document.getElementById('prefix-o'),
			mb_content = document.getElementById('prefix-oc'),
			viewX = Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
			viewY = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
			// use viewX for responsive if you want: 	if( $viewX < 768 ){}
			mb.style.height = mb_bg.style.height = mb_over.style.height = mb_content.style.height = viewY + 'px'; 
	</script>