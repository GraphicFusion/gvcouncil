<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
		<?php 
			$start_date = date('Ymd', current_time('timestamp')  );
			if( have_rows('dorado-events') ): ?>
				<h3>Dates:</h3>
				<ul>

				<?php while ( have_rows('dorado-events') ) : the_row(); 
					$acfdate = get_sub_field('dorado_event_date');
					$timeText = get_sub_field('dorado_event_time');
					$datestamp = strtotime( $acfdate );
					if( $startdate < acfdate ) : ?>

						<li><?php echo date( 'F j' , $datestamp ); ?> <?php echo $timeText; ?></li>
					<?php endif; ?>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
    </div>
    <footer>
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'dorado'), 'after' => '</p></nav>')); ?>
    </footer>
  </article>
<?php endwhile; ?>
