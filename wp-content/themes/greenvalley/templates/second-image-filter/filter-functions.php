<?php

if( function_exists('acf_add_local_field_group') ){
	$home_post = get_page_by_path('home');
	$home = 0;
	if( is_object( $home_post ) ){
		$home = $home_post->ID;
	}

	acf_add_local_field_group(
		array (
			'key' => 'second_filter_group',
			'title' => 'Second Welcome Block',
			'fields' => array (
				array (
					'key' => 'second_container_content',
					'label' => 'Content',
					'name' => 'second_container_content',
					'type' => 'wysiwyg' // or use text or textarea
				),
			),
			'location' => array (
				array(
					array (
						'param' => 'page',
						'operator' => '==',
						'value' => $home,
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
		)
	);
}
