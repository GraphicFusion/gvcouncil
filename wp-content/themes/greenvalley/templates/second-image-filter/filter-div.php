<?php 
global $images;
$rgba = implode ( ',' , getrgb( $images[2]['welcome_filter_color']));
?>
	<?php if(is_array( $images ) ) : ?>
		<div class="overlay-wrapper" id="welcome-three-ow">
			<div class="overlay-bg" id="welcome-three-obg" style="background-image: url('<?php echo $images[2]['welcome_image']; ?>');"></div>
			<div class='overlay' id="welcome-three-o" style="background: rgba(<?php echo $rgba; ?> , <?php echo $images[2]['welcome_filter_opacity']/100; ?>);"></div>
			<div class='overlay-content' id="welcome-three-oc">
				<div class='vert-center'>
					<div class="welcome-three-content welcome-one-secondary" >
			            <?php echo get_field('second_container_content'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>