<?php 
	$imageArray = get_field('vendor_container_image');
	$overlayColor = get_field('vendor_container_color');
	$rgba = implode ( ',' , getrgb( $overlayColor) );
?>

	<?php if(is_array( $imageArray ) ) : ?>
		<div class="overlay-wrapper" id="vendor-ow">
			<div class="overlay-bg" id="vendor-obg" style="background-image: url('<?php echo $imageArray['url']; ?>'); background-position: center; }"></div>
			<div class='overlay' id="vendor-o" style="background: rgba(<?php echo $rgba; ?> , <?php echo get_field('vendor_container_opacity')/100; ?>);"></div>
			<div class='overlay-content' id="vendor-oc">
				<div class="vendor-content">
		            <h2><?php echo get_field('vendor_container_content'); ?></h2>
					<div class='vc-links'>
						<!--<a href='/?geodir_signup=true' >Member Login</a>
						<div class='clearfix'></div>
						<a href='/?geodir_signup=true' class='gv-button'>Join or Login</a>-->
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<script>
		// get the height of the screen, subtract the navbar height, set the banner to the difference.
		function setVendor(){
			var	footerHeight = 62,
				mb = document.getElementById('vendor-ow'),
				headerHeight = document.getElementById('themeslug-navbar').clientHeight,
				mb_bg = document.getElementById('vendor-obg'),
				mb_over = document.getElementById('vendor-o'),
				mb_content = document.getElementById('vendor-oc'),
				viewX = Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
				viewY = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
				// use viewX for responsive if you want: 	if( $viewX < 768 ){}
				newHeight = ( viewY );
				if( viewX > 768 ){
					newHeight = newHeight - headerHeight - footerHeight;
					if( newHeight < 700 ){ newHeight = 700; }
				}
				mb.style.height = mb_bg.style.height = mb_over.style.height = mb_content.style.height = ( newHeight) + 'px'; 
		};
		setVendor();
	</script>