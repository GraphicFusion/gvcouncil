<?php
// search and replace "vendor" with unique id

add_action('wp_enqueue_scripts', 'vendor_load_filter_assets', 100);
function vendor_load_filter_assets(){
	wp_enqueue_style('vendor_filter_style', get_template_directory_uri(). '/templates/vendor-image-filter/filter-styles.css', false, null);
	wp_enqueue_script('vendor_filter_script', get_template_directory_uri(). '/templates/vendor-image-filter/filter-scripts.js', array(), null, true);
}

if( function_exists('acf_add_local_field_group') ){
	$home_post = get_page_by_path('home');
	$home = 0;
	if( is_object( $home_post ) ){
		$home = $home_post->ID;
	}

	acf_add_local_field_group(
		array (
			'key' => 'vendor_filter_group',
			'title' => 'Vendor Block',
			'fields' => array (
				array (
					'key' => 'vendor_container_content',
					'label' => 'Title',
					'name' => 'vendor_container_content',
					'type' => 'text' // or use text or textarea
				),
				array (
					'key' => 'vendor_container_image',
					'label' => 'Background Image',
					'instructions' => 'Size should be x by x',
					'name' => 'vendor_container_image',
					'type' => 'image',
					'return_format' => 'array'
				),
				array (
					'key' => 'vendor_container_color',
					'label' => 'Color',
					'instructions' => 'Set color of background image overlay',
					'name' => 'vendor_container_color',
					'type' => 'color_picker' 
				),
				array (
					'key' => 'vendor_container_opacity',
					'label' => 'Opacity',
					'instructions' => 'Set opacity of background image overlay (from 0 to 100 , where 0 is transparent)',
					'name' => 'vendor_container_opacity',
					'type' => 'number' 
				)
/*				array (
					'key' => 'vendor_container_hover_content',
					'label' => 'Hover State Content',
					'name' => 'vendor_container_hover_content',
					'type' => 'wysiwyg' // or use text or textarea
				),*/
			),
			'location' => array (
				array(
					array (
						'param' => 'page',
						'operator' => '==',
						'value' => $home,
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
		)
	);
}
