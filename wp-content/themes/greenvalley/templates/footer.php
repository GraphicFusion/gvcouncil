<footer class="content-info" role="contentinfo" id="page-footer">
	<div class='footer-nav'>
		<div class='container'>
	        <?php  if (has_nav_menu('footer-menu')) :
	                 wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'list-inline' ) );
			endif;?>
		</div>
	</div>
</footer>
