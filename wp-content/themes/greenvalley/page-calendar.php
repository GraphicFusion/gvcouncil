<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/page', 'header'); ?>
	<div class="default-content">
<?php // Show Calendar
			$events = new doradoEvents;
			$start = "";
			if( get_query_var('calendar_date' )){
				$requested_timestamp = strtotime( get_query_var('calendar_date' ) );
				$start = date( 'Ymd', $requested_timestamp);
			}
			$events->get_events($start,'' );
			$args = array(
				'range'			=> '1 month',
				'selected_time'	=> NULL,
				'start_type'	=> 'natural',
				'add_style'		=> 1,
				'debug'			=> 0
			);
			$cal = new calendar( $args );
			$cal->output_month();
			// print_r($cal);
			unset($cal);
			?>
		<?php get_template_part('templates/content', 'page'); ?>
	</div>
<?php endwhile; ?>
