<?php
	// Namespacing: this just names all the Dorado functions as "Dorado" 
	use Sonder\Dorado\Config; 	// see dorado/lib/config.php
	use Sonder\Dorado\Wrapper;	// see dorado/lib/wrapper.php

	$imageArray = get_field('banner_image');	
	if( is_array( $imageArray ) )  {
		$imgUrl = $imageArray['url'];
	}
	else{
		$imgUrl = get_template_directory_uri() . "/assets/img/banner_default.jpg";
	}
	$user_class = "";
	if( is_object( $current_user) ){
		foreach( $current_user->roles as $key=>$role){
			if( $role == 'gv_vendor' ){
				$user_class = 'user_vendor';
			}
			if( $role == 'gv_council' ){
				$user_class = 'user_council';
			}
			if( $role == 'gv_member' ){
				$user_class = 'user_hoa';
			}
			if( $role == 'administrator' ){
				$user_class = 'user_admin';
			}
		}
	};
?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?php wp_head(); ?>

	</head>
	<body <?php body_class( $user_class ); ?>>
		<!--[if lt IE 9]>
			<div class="alert alert-warning">
				<?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'dorado'); ?>	
			</div>
		<![endif]-->
	
		<?php
			do_action('get_header'); 				// add hook functionality to /lib/dorado_header_hook.php
			get_template_part('templates/header');
		?>
	
		<div class="wrap st-container stickyfooter st-effect-7 inactive" role="document" id="wrapper">
	        <div class="st-pusher row">
				<div id="sidebar-wrapper" class="sidebar-wrapper navbar-mobile st-menu st-effect-7" role="navigation">
					<?php get_template_part('templates/mobile', 'nav'); ?>
				</div>
				<div class="content st-content">
					<div class='st-content-inner'>
						<?php $useSidebar = 0; if (Config\display_sidebar()) : $useSidebar = 1; ?>
							<div class="container">
								<div class="row no-gutter">
									<div class="col-lg-12 banner-wrap">	
										<div class="header-banner" style="background-image: url('<?php echo  $imgUrl; ?>');"></div>
									</div>
									<div class="col-lg-9">					
										<main class="main" role="main">

											<?php include Wrapper\template_path(); ?>
						
										</main><!-- /.main -->
									</div><!-- /.col-lg -->
									<div class="col-lg-3">					
										<aside class="sidebar" role="complementary">
						
											<?php // include Wrapper\sidebar_path(); ?>
						
										</aside><!-- /.sidebar -->
									</div><!-- /.col-lg-3 -->	
								</div>
							</div>

						<?php else : ?>
							<div class="container">
								<div class="row no-gutter">
									<div class="col-lg-12 banner-wrap">	
										<div class="header-banner" style="background-image: url('<?php echo  $imgUrl; ?>');"></div>
									</div>
									<div class="col-lg-12">	
										<?php include Wrapper\template_path(); ?>
									</div><!-- /.col-lg-3 -->	
								</div>
							</div>							
						<?php endif; ?>
					</div><!-- /.st-content-inner -->
				</div><!-- /.content -->
			</div><!-- /.st-pusher -->
		</div><!-- /.wrap -->

		<?php
			do_action('get_footer');
			get_template_part('templates/footer');
			wp_footer();
		?>

	</body>
</html>