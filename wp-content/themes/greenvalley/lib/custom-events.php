<?php

/**
 * Event Custom Post Type
 */
add_action( 'init', 'register_event_post_type');
function register_event_post_type()
{ 
	register_post_type( 'event',
		array( 'labels' => 
			array(
				'name'               => 'Events',
				'singular_name'      => 'Event',
				'all_items'          => 'All Events',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Event',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Event',
				'new_item'           => 'New Event',
				'view_item'          => 'View Event',
				'search_items'       => 'Search Events',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Events post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-calendar',
			 'rewrite'	      => array( 'slug' => 'event', 'with_front' => false ),
			 'has_archive'      => 'events',
			'capability_type'     => 'page',
			'hierarchical'        => true,
			'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions')
		)
	);
}

/**
 * Event Categories
 */
register_taxonomy( 'event_cat', 
	array('event'),
	array('hierarchical' => true,
		'labels' => array(
			'name' 				=> 'Event Categories',
			'singular_name' 	=> 'Event Category',
			'search_items' 		=> 'Search Event Categories',
			'all_items' 		=> 'All Event Categories',
			'parent_item' 		=> 'Parent Event Category',
			'parent_item_colon' => 'Parent Event Category:',
			'edit_item' 		=> 'Edit Event Category',
			'update_item' 		=> 'Update Event Category',
			'add_new_item' 		=> 'Add New Event Category',
			'new_item_name' 	=> 'New Event Category Name',
		),
		'show_admin_column' => true, 
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'rewrite' 			=> array( 'slug' => 'event-cat' ),
	)
);

/* 
 * Events ACF 
 * */

if( function_exists('acf_add_local_field_group') ){

	acf_add_local_field_group(
		array (
			'key' => 'dorado-event-group',
			'title' => 'Event Info',
			'fields' => array (
				array (
					'key' => 'dorado-events',
					'label' => 'Date/Times',
					'name' => 'dorado-events',
					'type' => 'repeater'
				)
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'event',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
		)
	);

	// Adds repeater rows to testimonials group
	acf_add_local_field_group(
		array (
			'key' => 'dorado-repeater-row',
			'title' => 'Row',
			'parent' => 'dorado-events',
			'fields' => array (
				array (
					'key' => 'dorado_event_date',
					'label' => 'Date',
					'name' => 'dorado_event_date',
					'type' => 'date_picker',
					'display_format' => 'm/d/Y',
					'return_format' => 'Ymd',
					'parent' => 'dorado-events'
				),
				array (
					'key' => 'dorado_event_time',
					'label' => 'Times',
					'name' => 'dorado_event_time',
					'type' => 'text',
					'parent' => 'dorado-events'
				),
/*				array (
					'key' => 'dorado-event-active',
					'label' => 'Active',
					'name' => 'dorado-event-active',
					'type' => 'true_false',
					'parent' => 'dorado-events'
				),*/
			)
		)
	);
}
function acf_lookup( $where ){
	$where = str_replace("meta_key = 'dorado-events_%_dorado_event_date'", "meta_key LIKE 'dorado-events_%_dorado_event_date'", $where);
	return $where;
} 
add_filter("posts_where", "acf_lookup");

/**
  * EVENTS Class 
  *
  * Returns all events, recurring and fixed, sorted chronologically from today
  * 
  * @start_date string 	optional - this marks the beginning of the time frame of interest in the form 'YYYYMMDD'; default is current time;
  * @max_limit integer 	optional - number of days to search from $start_date
  * @event_cat string 	optional - category slug to limit search to
  * 
  * @return array of key = event's timestamp and value = array of event's post id; array is sorted with recurring events populated for each occurence. 
  * 			Array(
  *				    [1409529600] => Array
  *				        (
  *				            [0] => 299
  *				        )
  *				
  *				    [1410912000] => Array
  *				        (
  *				            [0] => 296
  *				            [1] => 298
  *				        )
  *
  *					)
  * 
  */
class doradoEvents{

	function get_events( $start_date = NULL, $max_limit = 0, $event_cat = NULL , $max_number = null , $includeEvents = null ){
		global $wpdb;
		if( !$start_date ){ $start_date = date('Ymd', current_time('timestamp')  ); }
		$stamp = strtotime( $start_date );
		$max_time = date( 'Ymd', ( $stamp + $max_limit * 86400 ) );
		$single_events = array();
		$recurring_events = array();
		$total_events = 0; 
		$args = array(
			'numberposts' => -1,
			'post_type' => 'event',
			'posts_per_page' => -1,
			'meta_query' => array(
				array(
					'key' => 'dorado-events_%_dorado_event_date',
					'value' => $start_date,
					'type' => 'NUMERIC',
					'compare'=> '>='
					
				)
			)
		);
		if($max_limit ){
			$args['meta_query'][] = 	
				array(
					'key' => 'dorado-events_%_dorado_event_date',
					'value' => $max_time,
					'type' => 'NUMERIC',
					'compare'=> '<'
				);
		}
		if($event_cat){ 
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'event_cat',
					'field' => 'slug',
					'terms' => $event_cat
				)
			);	
			
		}
		if($includeEvents){
			$args['post__in'] = $includeEvents;
		}
		$the_query = new WP_Query( $args );
		if( $the_query->have_posts() ){
			foreach ( $the_query->posts as $spost ) {
				$count = 0;
				while ( $event_date = get_post_meta( $spost->ID , "dorado-events_". $count ."_dorado_event_date", true) ){ 
					$eventstamp = strtotime( $event_date );
//						echo "eventstamp:".$eventstamp."<br>";
//						echo "stamp:".$stamp."<br>";
					$count++;
					if( $eventstamp >= $stamp ){ 
						$time = get_post_meta( $spost->ID , "dorado-events_". $count ."_dorado_event_time", true);
						//$eventstamp = $eventstamp + $time * 60 * 60;
						$single_events[$eventstamp][] = array('post'=> $spost->ID , 'event_times' => $time);
						$total_events++;
					}
				}
			}
		}
/*		if( $max_limit > 1 ){
			$meta_query = array( 'key' => 'recurring_day'); 
		}
		else{
			$meta_query = array(
							'key' => 'recurring_day',
							'value' => date ( 'w' , $stamp),
							'compare' => '=',
							'type' => 'numeric',
				       );
		}		
		$recurring_query = new WP_Query( array (
						    'post_type' => 'event',
						    'meta_key' => 'recurring_day',
						    'orderby' => 'meta_value_num',
						    'order' => 'ASC',
						    'meta_query' => array( $meta_query )
						));	
		if( $max_limit > 1 ){
			// $recurring count is the number of times the recurring day occurs in the period of interest
			$recurring_count = floor($max_limit/ 7);
			foreach($recurring_query->posts as $re_key => $re_post){
				$recurring_day = get_field( 'recurring_day', $re_post->ID );
				$dif_num = $recurring_day - date('w');
				if( $dif_num < 0 ){ $dif_num = $dif_num + 7;}
				$i = 0;		
				while($i < $recurring_count){
					$recurring_timestamp = strtotime( 'today 12:00am +' . $dif_num .' day');
					$recurring_events[$recurring_timestamp][] = $re_post->ID;
					$dif_num = $dif_num +7;
					$i++;
					$total_events++;
				}			
			}
		}
		else{
			foreach($recurring_query->posts as $re_key => $re_post){
				$recurring_day = get_field( 'recurring_day', $re_post->ID );
				$recurring_events[$stamp][] = $re_post;
				$total_events++;					
			}	
		}
		$all_events = $single_events + $recurring_events;*/
		$all_events = $single_events;
		ksort($all_events);
		if (count($single_events)>0 && $max_number > 0){
			$i = 0;
			while($i<$max_number){
				foreach( $all_events as $timestamp=>$event_array){
					foreach( $event_array as $key=>$event_id ){
						if($i < $max_number){
							$new_array[$timestamp][] = $event_id;
							if($i == $total_events -1 ){ $i = $max_number; }							
							$i++;
						}
					} 
				}
			
			}
			$all_events = $new_array;
		}
		$this->events = $all_events;
	}
}
function showEventBlock( $eventObj ){
	// Event Block will grab the FIRST category; only set one
	$id = $eventObj->ID;
	$terms = get_the_terms( $id, 'event_cat' );
	$cat = $terms[0];
	if( have_rows('dorado-events' , $id) ): $i = 0;
	    while ( have_rows('dorado-events', $id) && !$i ) : the_row();	
	        // display a sub field value
	        $date = strtotime( get_sub_field('dorado_event_date') ); // convert date to unix; output by acf as Ymd: 20150618
			if ( $date ){
				$date = date( 'F j, Y' , $date);
			}
	        $time = the_sub_field('dorado_event_time');
			$i = 1;
	    endwhile;	
	endif;
?>
	<div class='event-block'>
		<div class='label-block'><?php echo $cat->name; ?></div>
		<div class='content-block'>
			<div class='overlay-wrapper'>
				<div class='overlay-bg' style="background:url(<?php $bgArray = get_field('featured-event-image', $id); echo $bgArray['url']; ?>)"></div>
				<div class='overlay'></div>
				<div class='overlay-content'>
					<label><?php echo $eventObj->post_title; ?></label>
					<div class='date'>
						<?php echo $date; ?>			
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
}
/**
 * FILTERS
 **/

add_filter('rewrite_rules_array', 'add_event_rewrite_rules');
add_filter('query_vars', 'add_event_query_vars');
  
// Adds a url query to pass date requests in the URL (i.e., example.net/?var1=value1&calendar_date=2014-08
function add_event_query_vars($aVars) {
	$aVars[] = "event_page";
	$aVars[] = "search_terms";
	return $aVars;
}

// ON INSTALL SAVE Permalinks or flush_rewrite_rules();
function add_event_rewrite_rules($aRules) {
	$aNewRules = array('events/page/?([^/]*)/?([^/]*)' => 'index.php?post_type=event&event_page=$matches[1]&search_terms=$matches[2]');
	$aRules = $aNewRules + $aRules;
	return $aRules;
}
//add_filter( 'redirect_canonical','event_disable_redirect_canonical' );
function event_disable_redirect_canonical( $redirect_url ){
    global $post;
    $ptype = get_post_type( $post );
    if ( $ptype == 'event' && is_archive() ) $redirect_url = false;
    return $redirect_url;
}

