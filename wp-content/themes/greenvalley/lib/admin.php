<?php

add_action( 'admin_init', 'egg_dependencies' );

/**
 * Check for dependencies
 */
function egg_dependencies()
{
	if ( ! class_exists( 'acf' ) )
	{
		add_action( 'admin_notices', 'egg_acf_dependency_message' );
	}
}


/**
 * Add a nag for required dependencies that are missing
 */
function egg_acf_dependency_message() { ?>
	<div class="update-nag">
		This theme requires the <a href="http://wordpress.org/plugins/advanced-custom-fields/">Advanced Custom Fields</a> plugin to be installed and activated.
	</div>
<?php } 

	/**
	 * Setup Plugin List to only show for Admin
	 */
	function hidePlugins() {
		global $wp_list_table;
		$hidearr = array(
			'wp-migrate-db-pro/wp-migrate-db-pro.php',
			'advanced-custom-fields-pro/acf.php'
		);
		$myplugins = $wp_list_table->items;
		foreach ($myplugins as $key => $val) {
			if (in_array($key,$hidearr)) {
				unset($wp_list_table->items[$key]);
			}
		}
	}
	$admin = false;
	if ( is_user_logged_in() ) {
		global $user_id;
		$user = new WP_User( $user_ID );
		if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
			foreach ( $user->roles as $role )
				if( $role == 'administrator' ){
					$admin = true;
				}
		}
	}
	if( !$admin) {
		add_action( 'pre_current_active_plugins', 'hidePlugins' );
	}
add_role(
    'gv_vendor',
    __( 'Vendor' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
        'delete_posts' => false, // Use false to explicitly deny
    )
);
add_role(
    'gv_member',
    __( 'HOA Member' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => false,
        'delete_posts' => false, // Use false to explicitly deny
    )
);
add_role(
    'gv_council',
    __( 'Council Member' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
        'delete_posts' => false, // Use false to explicitly deny
    )
);

add_action( 'admin_menu', 'register_expired_lic' );

function register_expired_lic() {
	add_menu_page( 'Expired Licenses', 'Expired Licenses', 'manage_options', 'expired-licenses', 'outputLic' );

}
function outputLic(){
	$date = date('Ymd');
	$posts = get_posts(array(
		'numberposts'	=> -1,
		'post_type'		=> 'gd_place',
		'meta_query'	=> array(
			'relation'		=> 'AND',
			array(
				'key'	 	=> 'license_expiration',
				'value'	  	=> '0',
				'compare' 	=> '>',
			),
			array(
				'key'	  	=> 'license_expiration',
				'value'	  	=> $date,
				'compare' 	=> '<',
			),
		),
	)); ?>
	<h1>Expired Licenses</h1>
	<?php if(count($posts)>0) : ?>
		<ul>
			<?php foreach( $posts as $post) : ?>
					<?php 
						$exp_arr = get_post_meta($post->ID,'license_expiration'); 
						$timestamp = strtotime( $exp_arr[0] );

					?>
					<li><a href="/wp-admin/post.php?post=<?php echo $post->ID; ?>&action=edit"><?php echo $post->post_title; ?></a> | Expired: <?php echo date( 'm/d/Y',$timestamp); ?></li>	
			<?php	endforeach; ?>
		</ul>
	<?php else: ?>
		There are no Vendors with licences that are expired currently.
	<?php endif; ?>
<?php
}