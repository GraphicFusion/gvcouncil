<?php

/**
 * council Custom Post Type
 */
add_action( 'init', 'register_council_post_type');
function register_council_post_type()
{ 
	register_post_type( 'council',
		array( 'labels' => 
			array(
				'name'               => 'Council Page',
				'singular_name'      => 'Council Page',
				'all_items'          => 'All Council Pages',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Council Page',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Council Page',
				'new_item'           => 'New Council Page',
				'view_item'          => 'View Council Page',
				'search_items'       => 'Search Council Pages',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Council Content',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-calendar',
			 'rewrite'	      => array( 'slug' => 'council', 'with_front' => false ),
			 'has_archive'      => 'councils',
			'capability_type'     => 'page',
			'hierarchical'        => true,
			'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions')
		)
	);
}

/**
 * council Categories
 */
register_taxonomy( 'council_cat', 
	array('council'),
	array('hierarchical' => true,
		'labels' => array(
			'name' 				=> 'Council Categories',
			'singular_name' 	=> 'Council Category',
			'search_items' 		=> 'Search Council Categories',
			'all_items' 		=> 'All Council Categories',
			'parent_item' 		=> 'Parent Council Category',
			'parent_item_colon' => 'Parent Council Category:',
			'edit_item' 		=> 'Edit Council Category',
			'update_item' 		=> 'Update Council Category',
			'add_new_item' 		=> 'Add New Council Category',
			'new_item_name' 	=> 'New Council Category Name',
		),
		'show_admin_column' => true, 
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'rewrite' 			=> array( 'slug' => 'council-cat' ),
	)
);



/**
  * councilS Class 
  *
  * Returns all councils, recurring and fixed, sorted chronologically from today
  * 
  * @start_date string 	optional - this marks the beginning of the time frame of interest in the form 'YYYYMMDD'; default is current time;
  * @max_limit integer 	optional - number of days to search from $start_date
  * @council_cat string 	optional - category slug to limit search to
  * 
  * @return array of key = council's timestamp and value = array of council's post id; array is sorted with recurring councils populated for each occurence. 
  * 			Array(
  *				    [1409529600] => Array
  *				        (
  *				            [0] => 299
  *				        )
  *				
  *				    [1410912000] => Array
  *				        (
  *				            [0] => 296
  *				            [1] => 298
  *				        )
  *
  *					)
  * 
  */
class doradocouncils{

	function get_councils( $start_date = NULL, $max_limit = 0, $council_cat = NULL , $max_number = null , $includecouncils = null ){
		global $wpdb;
		if( !$start_date ){ $start_date = date('Ymd', current_time('timestamp')  ); }
		$stamp = strtotime( $start_date );
		$max_time = date( 'Ymd', ( $stamp + $max_limit * 86400 ) );
		$single_councils = array();
		$recurring_councils = array();
		$total_councils = 0; 
		$args = array(
			'numberposts' => -1,
			'post_type' => 'council',
			'meta_query' => array(
				array(
					'key' => 'dorado-councils_%_dorado_council_date',
					'value' => $start_date,
					'type' => 'NUMERIC',
					'compare'=> '>='
					
				)
			)
		);
		if($max_limit ){
			$args['meta_query'][] = 	
				array(
					'key' => 'dorado-councils_%_dorado_council_date',
					'value' => $max_time,
					'type' => 'NUMERIC',
					'compare'=> '<'
				);
		}
		if($council_cat){ 
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'council_cat',
					'field' => 'slug',
					'terms' => $council_cat
				)
			);	
			
		}
		if($includecouncils){
			$args['post__in'] = $includecouncils;
		}
		$the_query = new WP_Query( $args );
		if( $the_query->have_posts() ){
			foreach ( $the_query->posts as $spost ) {
				$count = 0;
				if( get_field('council_dates', $spost->ID ) ){
					while ( $council_date = get_post_meta( $spost->ID , "dorado-councils_". $count ."_dorado_council_date", true) ){ 
						$councilstamp = strtotime( $council_date );
						$time = get_post_meta( $spost->ID , "dorado-councils_". $count ."_dorado_council_time", true);
						//$councilstamp = $councilstamp + $time * 60 * 60;
						$single_councils[$councilstamp][] = array('post'=> $spost->ID , 'council_times' => $time);
						$count++;
						$total_councils++;
					}
				}
			}
		}
/*		if( $max_limit > 1 ){
			$meta_query = array( 'key' => 'recurring_day'); 
		}
		else{
			$meta_query = array(
							'key' => 'recurring_day',
							'value' => date ( 'w' , $stamp),
							'compare' => '=',
							'type' => 'numeric',
				       );
		}		
		$recurring_query = new WP_Query( array (
						    'post_type' => 'council',
						    'meta_key' => 'recurring_day',
						    'orderby' => 'meta_value_num',
						    'order' => 'ASC',
						    'meta_query' => array( $meta_query )
						));	
		if( $max_limit > 1 ){
			// $recurring count is the number of times the recurring day occurs in the period of interest
			$recurring_count = floor($max_limit/ 7);
			foreach($recurring_query->posts as $re_key => $re_post){
				$recurring_day = get_field( 'recurring_day', $re_post->ID );
				$dif_num = $recurring_day - date('w');
				if( $dif_num < 0 ){ $dif_num = $dif_num + 7;}
				$i = 0;		
				while($i < $recurring_count){
					$recurring_timestamp = strtotime( 'today 12:00am +' . $dif_num .' day');
					$recurring_councils[$recurring_timestamp][] = $re_post->ID;
					$dif_num = $dif_num +7;
					$i++;
					$total_councils++;
				}			
			}
		}
		else{
			foreach($recurring_query->posts as $re_key => $re_post){
				$recurring_day = get_field( 'recurring_day', $re_post->ID );
				$recurring_councils[$stamp][] = $re_post;
				$total_councils++;					
			}	
		}
		$all_councils = $single_councils + $recurring_councils;*/
		$all_councils = $single_councils;
		ksort($all_councils);
		if (count($single_councils)>0 && $max_number > 0){
			$i = 0;
			while($i<$max_number){
				foreach( $all_councils as $timestamp=>$council_array){
					foreach( $council_array as $key=>$council_id ){
						if($i < $max_number){
							$new_array[$timestamp][] = $council_id;
							if($i == $total_councils -1 ){ $i = $max_number; }							
							$i++;
						}
					} 
				}
			
			}
			$all_councils = $new_array;
		}
		$this->councils = $all_councils;
	}
}
function showcouncilBlock( $councilObj ){
	// council Block will grab the FIRST category; only set one
	$id = $councilObj->ID;
	$terms = get_the_terms( $id, 'council_cat' );
	$cat = $terms[0];
	if( have_rows('dorado-councils' , $id) ): $i = 0;
	    while ( have_rows('dorado-councils', $id) && !$i ) : the_row();	
	        // display a sub field value
	        $date = strtotime( get_sub_field('dorado_council_date') ); // convert date to unix; output by acf as Ymd: 20150618
			if ( $date ){
				$date = date( 'F j, Y' , $date);
			}
	        $time = the_sub_field('dorado_council_time');
			$i = 1;
	    endwhile;	
	endif;
?>
	<div class='council-block'>
		<div class='label-block'><?php echo $cat->name; ?></div>
		<div class='content-block'>
			<div class='overlay-wrapper'>
				<div class='overlay-bg' style="background:url(<?php $bgArray = get_field('featured-council-image', $id); echo $bgArray['url']; ?>)"></div>
				<div class='overlay'></div>
				<div class='overlay-content'>
					<label><?php echo $councilObj->post_title; ?></label>
					<div class='date'>
						<?php echo $date; ?>			
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
}
/**
 * FILTERS
 **/

add_filter('rewrite_rules_array', 'add_council_rewrite_rules');
add_filter('query_vars', 'add_council_query_vars');
  
// Adds a url query to pass date requests in the URL (i.e., example.net/?var1=value1&calendar_date=2014-08
function add_council_query_vars($aVars) {
	$aVars[] = "council_page";
	$aVars[] = "search_terms";
	$aVars[] = "sign_in";
	$aVars[] = "request";
	return $aVars;
}

// ON INSTALL SAVE Permalinks or flush_rewrite_rules();
function add_council_rewrite_rules($aRules) {
	$aNewRules = array('councils/page/?([^/]*)/?([^/]*)' => 'index.php?post_type=council&council_page=$matches[1]&search_terms=$matches[2]');
	$aRules = $aNewRules + $aRules;
	return $aRules;
}
//add_filter( 'redirect_canonical','council_disable_redirect_canonical' );
function council_disable_redirect_canonical( $redirect_url ){
    global $post;
    $ptype = get_post_type( $post );
    if ( $ptype == 'council' && is_archive() ) $redirect_url = false;
    return $redirect_url;
}

