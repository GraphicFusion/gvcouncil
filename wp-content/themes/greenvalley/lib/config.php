<?php

namespace Sonder\Dorado\Config;

use Sonder\Dorado\ConditionalTagCheck;

/**
 * Enable theme features
 */
add_theme_support('soil-clean-up');         // Enable clean up from Soil
add_theme_support('soil-nav-walker');       // Enable cleaner nav walker from Soil
add_theme_support('soil-relative-urls');    // Enable relative URLs from Soil
add_theme_support('soil-nice-search');      // Enable nice search from Soil
add_theme_support('soil-jquery-cdn');       // Enable to load jQuery from the Google CDN

/**
 * Configuration values
 */
if (!defined('WP_ENV')) {
  // Fallback if WP_ENV isn't defined in your WordPress config
  // Used in lib/assets.php to check for 'development' or 'production'
  define('WP_ENV', 'production');
}

if (!defined('DIST_DIR')) {
  // Path to the build directory for front-end assets
  define('DIST_DIR', '/assets/');
}

/**
 * Define which pages shouldn't have the sidebar
 */
function display_sidebar() {
  static $display;
  if (!isset($display)) {
    $conditionalCheck = new ConditionalTagCheck(
      /**
       * Any of these conditional tags that return true won't show the sidebar.
       * You can also specify your own custom function as long as it returns a boolean.
       *
       * To use a function that accepts arguments, use an array instead of just the function name as a string.
       *
       * Examples:
       *
       * 'is_single'
       * 'is_archive'
       * array('is_page', 'about-me')
       * ['is_tax', ['flavor', 'mild']]
       * ['is_page_template', 'about.php']
       * ['is_post_type_archive', ['foo', 'bar', 'baz']]
       *
       */
      array(
        'is_404',
        'is_front_page',
      )
    );

//    $display = apply_filters('dorado/display_sidebar', $conditionalCheck->result);
	$display= 0;	
	$post_type = get_query_var('post_type');
	if( $post_type == 'gd_place' ){
		$display = 0;
	}
	if( $post_type == 'event' ){
		$display = 0;
	}

	global $post;
	// check if post/page has the acf field 'dorado_add_sidebar'; see lib/acf-site-settings.php;
	if( is_object( $post ) ){ 
		$acf_exists = get_post_meta($post->ID, 'dorado_add_sidebar', false);
		if( count( $acf_exists ) > 0 ){
			// if post/page has field the field declaration takes precedence over conditionalCheck; 
			if ( get_field( 'dorado_add_sidebar', $post->ID) ){
				$display = 1;
			}
			else{
				$display = 0;
			}
		}
	}
  }
  return $display;
}