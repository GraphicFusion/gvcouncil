<?php
/* 
 * Options Page
 * Add ACF items to an options page 
 * */

if ( function_exists('acf_add_options_page') ) {

    acf_add_options_page(
        array(
            'page_title'    => 'HOA',
            'menu_title'    => 'HOA',
            'menu_slug'     => 'hoa-list',
            'capability'    => 'edit_posts',
            'parent_slug'   => '',
            'position'      => false,
            'icon_url'      => false
        )
    );

}
if( function_exists('acf_add_local_field_group') ){
	$home_post = get_page_by_path('home');
	$home = 0;
	if( is_object( $home_post ) ){
		$home = $home_post->ID;
	}

	acf_add_local_field_group(
		array (
			'key' => 'dorado_sidebar_group',
			'title' => 'Sidebar',
			'fields' => array (
/*				array (
					'key' => 'dorado_sidebar',
					'label' => 'Sidebar Repeater',
					'name' => 'dorado_sidebar',
					'type' => 'repeater'
				),*/
				array (
					'key' => 'dorado_add_sidebar',
					'label' => 'Add Sidebar',
					'instructions' => 'Leave unchecked for full width page with no sidebar',
					'name' => 'dorado_add_sidebar',
					'type' => 'true_false'
				),
/*				array (
					'key' => 'use_custom_sidebar',
					'label' => 'Use Custom Sidebar',
					'instructions' => 'Leave unchecked to use the default sidebar',
					'name' => 'use_custom_sidebar',
					'type' => 'true_false',
				)*/
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
					),
					array (
						'param' => 'page',
						'operator' => '!=',
						'value' => $home,
					),
				),
				array(
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'post',
					),
				),
				array(
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'gd_place',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
		)
	);

	// Adds repeater rows to testimonials group
	acf_add_local_field_group(
		array (
			'key' => 'dorado_repeater_row',
			'title' => 'Row',
			'parent' => 'dorado_sidebar',
			'fields' => array (
				array (
					'key' => 'dorado_event_time',
					'label' => 'Times',
					'name' => 'dorado_event_time',
					'type' => 'text',
					'parent' => 'dorado_sidebar'
				),
				array (
					'key' => 'dorado-event-active',
					'label' => 'Active',
					'name' => 'dorado-event-active',
					'type' => 'true_false',
					'parent' => 'dorado_sidebar'
				),
			)
		)
	);
}
function acf_load_sidebar_choices( $field ) {
    
	$nameField = get_field_object('sb_block_name');
	$key = $nameField['key']; 

    // reset choices
    $field['choices'] = array( 'events'=>'Events' , 'donate'=>'Donate');
   
    // get the sidebar block name from options page without any formatting
    $choices = get_field('sb_flexible_content', 'option', false);
	foreach( $choices as $i => $array){
		$name = $array[$key];
		$field['choices'][ $name ] = $name;
	}      
    return $field;
}
add_filter('acf/load_field/name=sb_builder_block', 'acf_load_sidebar_choices');

function get_acf_sidebar( $post_id ){
	if( get_field('add_sidebar' , $post_id ) ){
		// check if default
		$use_custom_sidebar = get_field('use_custom_sidebar' , $post_id );
		if( $use_custom_sidebar ){
			$type = $post_id;
		}
		else{	
			$type = 'option';
		}
		if( have_rows('sidebar_builder_blocks', $type) ):
		    while ( have_rows('sidebar_builder_blocks', $type) ) : the_row();
	
		        $block_names[] = get_sub_field('sb_builder_block');
		
		    endwhile;
		
		else :
		
		    // no rows found
		
		endif;
		foreach($block_names as $key => $block_name){
			if( $block_name == 'events'){
				$blocks[] = 'events';
			}
			elseif( $block_name == 'donate' ){
				$blocks[] = 'donate';
			}
			else{
				$blocks[] = get_sidebar_block( $block_name );	
			}
		}	
		return $blocks;
	}
	else{
		return false;
	}
}
function get_sidebar_block( $name ){
    $defined_blocks = get_field('sb_flexible_content', 'option', false);
	$nameField = get_field_object('sb_block_name');
	$key = $nameField['key']; 
	foreach( $defined_blocks as $i => $block ){
		if( $name == $block[$key] ){
			$returnBlock = $block;
			break;
		}
	}
	return $returnBlock;
}
function get_event_block(){
	$block = array();

	return $block;
}
function build_sidebar( $sidebar ){
	$nameField = get_field_object('sb_block_name');
	$nameKey = $nameField['key']; 

	$titleField = get_field_object('sb_block_title');
	$title = $titleField['key']; 

	$textField = get_field_object('sb_text_repeater');
	$textRows = $textField['key']; 

	$textField = get_field_object('sb_text_content');
	$text = $textField['key']; 

	$textFontField = get_field_object('sb_text_font');
	$textFont = $textFontField['key']; 

	$textColorField = get_field_object('sb_text_color');
	$textColor = $textColorField['key']; 

	$infoField = get_field_object('sb_info_repeater');
	$info = $infoField['key']; 

	$infoLabel = get_field_object('sb_info_label');
	$label = $infoLabel['key']; 

	$labelFontField = get_field_object('sb_label_font');
	$labelFont = $labelFontField['key']; 

	$labelColorField = get_field_object('sb_label_color');
	$labelColor = $labelColorField['key']; 

	$infoValue = get_field_object('sb_info_value');
	$value = $infoValue['key']; 

	$valueFontField = get_field_object('sb_value_font');
	$valueFont = $valueFontField['key']; 

	$valueColorField = get_field_object('sb_value_color');
	$valueColor = $valueColorField['key']; 

	foreach( $sidebar as $i => $block ): 
		if( $block == 'events' ): ?>

			<div class='sidebar-block events'>
				<div class='sidebar-title'>Events</div>
				  	  <?php get_template_part('templates/content', 'events'); ?>
				<div class='sidebar-text'></div>
			</div>
		
		<?php elseif( $block == 'donate') : ?>

			<div class='sidebar-block donate'>
				<div class='sidebar-title'>Donate/Support</div>
				<div class='sidebar-text'>
					<label>Choose Your Amount</label>	
					<?php
						$donate_page_id = get_page_by_title('donate'); 
						$buttons = get_field('buttons' , $donate_page_id );
						if($buttons) { 
							$out = "<select id='donate_option'>";
							foreach( $buttons as $button ){
								$out .= "<option value='". $button['donation_link'] ."'>". $button['donation_amount'] . " </option>";
							}
							$out .= "</select>";
							echo $out;
						}
					?>
					<div class='sidebar-title' id='submit_donate'>Donate</div>
				</div>
			</div>

		<?php else : ?>

		<div class='sidebar-block'>
			<div class='sidebar-title'><?php echo $block[$title]; ?></div>
			<div class='sidebar-text'>

			<?php if( is_array ( $block[$info] ) ): ?>
				<?php foreach( $block[$info] as $row ): ?>

					<div class='info-row row'>
						<div class='col-xs-12 col-sm-6 col-lg-3'>
							<label class='<?php echo $row[$labelFont]. " " .$row[$labelColor]; ?>'><?php echo $row[$label]; ?></label>
						</div>
						<div class='col-xs-12 col-sm-6 col-lg-9'>
							<div class='info-text <?php echo $row[$valueFont]. " " .$row[$valueColor]; ?>'><?php echo $row[$value]; ?></div>
						</div>
					</div>
				
				<?php endforeach; ?>
			<?php endif; ?>

			<?php if( is_array ( $block[$textRows] ) ): ?>
				<?php foreach( $block[$textRows] as $row ): ?>

					<div class='text-row'>
						<div class='text-content <?php echo $row[$textFont]. " " .$row[$textColor]; ?>'><?php echo $row[$text]; ?></div>
					</div>
				
				<?php endforeach; ?>
			<?php endif; ?>

			</div>
		</div>	
		
	<?php endif; endforeach; 
}
?>