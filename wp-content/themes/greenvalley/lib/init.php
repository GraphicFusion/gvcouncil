<?php

namespace Sonder\Dorado\Init;

use Sonder\Dorado\Assets;

/**
 * Theme setup
 */
function setup() {
  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('dorado', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus( array(
    'primary_navigation' => __('Primary Navigation', 'dorado')
  ) );

  // Add post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Add post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio'));

  // Add HTML5 markup for captions
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', array('caption', 'comment-form', 'comment-list'));

  // Tell the TinyMCE editor to use a custom stylesheet
  add_editor_style(Assets\asset_path('styles/editor-style.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar( array(
    'name'          => __('Primary', 'dorado'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ));

}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

//========= FOOTER MENU ===============================================
function register_footer_menu() {
    register_nav_menu('footer-menu',__( 'Footers Menu' ));
}
add_action( 'init', 'register_footer_menu' );

function redirect_signin(){
	$signin = false;
	if( array_key_exists('geodir_signup', $_GET ) || array_key_exists('geodir_signin', $_GET )){
		$signin = true;
	}
	if ( is_user_logged_in() && $signin ){
		global $current_user;
		$roles = $current_user->roles;
		foreach( $roles as $role ){
			if( $role == 'subscriber'){
				wp_redirect ( home_url("/vendors") );
				exit;
			}
		}
	}
}
add_action( 'init', function(){
	redirect_signin();
});