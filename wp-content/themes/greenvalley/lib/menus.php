<?php
	//========= FOOTER MENU ===============================================
	function register_footer_menu() {
	    register_nav_menu('footer-menu',__( 'Footer Menu' ));
	}
	add_action( 'init', 'register_footer_menu' );
	
	//========= SECONDARY MOBILE MENU =====================================
	function register_mobile_menu() {
	    register_nav_menu('mobile-menu',__( 'Mobile Menu' ));
	}
	add_action( 'init', 'register_mobile_menu' );
?>