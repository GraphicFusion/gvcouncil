<?php
function getrgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}

add_filter( 'update_user_metadata', 'myplugin_update_foo', 10, 5 );


function myplugin_update_foo( $null, $object_id, $meta_key, $meta_value, $prev_value ) {

	if ( 'first_name' == $meta_key && array_key_exists('user_hoa', $_POST ) ) {
		$hoa = $_POST['user_hoa'];
		if( $hoa ){
	        update_user_meta($object_id, 'hoa', $hoa);
		}
	}
	return null; // this means: go on with the normal execution in meta.php

}
remove_role( 'subscriber' );
//add columns to User panel list page
function add_user_columns($column) {
    $column['user_hoa'] = 'HOA';
    $column['vendor'] = 'Vendor';

    return $column;
}
add_filter( 'manage_users_columns', 'add_user_columns' );
//add the data
function add_user_column_data( $val, $column_name, $user_id ) {
	$meta = get_user_meta($user_id); 

    switch ($column_name) {
        case 'user_hoa' :
			if( array_key_exists ('hoa', $meta ) ){
				$hoa = $meta['hoa'][0]; 
	            return $hoa;
			}
            break;
        case 'vendor' :
			if( array_key_exists ('hoa', $meta ) ){
				$vendor_id = $meta['vendor'][0];
				$post = get_post( $vendor_id);
				if( is_object( $post ) ){
					$vendor = $post->post_title;
    	        	return $vendor;
				}
            	break;
			}
        default:
    }
    return;
}
add_filter( 'manage_users_custom_column', 'add_user_column_data', 10, 3 );

add_filter('acf/load_field/name=user_hoa', 'acf_load_hoa_field_choices');
function acf_load_hoa_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();

    // get the textarea value from options page without any formatting
    $choices = get_field('hoas', 'option', false);
    
    // loop through array and add to field 'choices'
    if( is_array($choices) ) {
        foreach( $choices as $choice_arr ) {
			foreach( $choice_arr as $choice ){
	            $field['choices'][ $choice ] = $choice;
    		}        
        }
        
    }
    return $field;    
}

// Redirect vendor when logged in so vendor only sees their ad
add_filter('wp', 'user_redirect');
function user_redirect() {
	global $current_user;
	$member_page = get_page_by_path( 'member' );
		
	if( isset( $_GET['logged_in_via'] ) ){
		foreach( $current_user->roles as $role ){
			if ( 'gv_member' == $role ){
				$url = get_permalink( $member_page->ID );
				wp_redirect( $url );
			}
			if ( 'administrator' == $role ){
				wp_redirect( '/wp-admin/');
			}
			if ( 'gv_vendor' == $role ){
				wp_redirect( '/vendors/');
			}
		}		
	}	
	if( !is_home() && ( !isset($GET['geodir_signup'])) ){
		$thisPage = get_queried_object();
		if( get_field('member_role_only') && !is_user_logged_in() ){
	 		wp_redirect( '/?geodir_signup=true');		
		}
		if ( !is_user_logged_in() && ( get_post_type() == 'gd_place'  || $thisPage->ID == $member_page->ID  || $thisPage->post_parent == $member_page->ID ) ) {
	 		wp_redirect( '/?geodir_signup=true');
		}
		$post_id = get_the_id();  
		foreach( $current_user->roles as $role ){
			if ( 'gv_vendor' == $role ){
				$place_id = get_user_meta($current_user->ID,'vendor',true);
				$place = get_post( $place_id );
				if( is_archive('gd_place') || ( is_object( $place ) && ( $place_id != $post_id) && get_post_type() == 'gd_place' )){
					if( $place_id ){
						wp_redirect( $place->guid );
					}
				}
			}
		}		
	}
}
// Redirect to home page on logout
add_action('wp_logout','go_home');
function go_home(){
  wp_redirect( home_url() );
  exit();
}


?>