<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
        <?php _e('Sorry, no results were found.', 'dorado'); ?>
    </div>

    <?php get_search_form(); ?>
<?php endif; ?>
	<?php
		
		while ($wp_query ->have_posts() ) :	$wp_query->the_post(); 
	?>
			<article <?php post_class(); ?>>
				<div class='row'>
				<div class="col-lg-3">
					<?php the_post_thumbnail('thumbnail'); ?>
				</div>
				<div class="col-lg-9">
					<header>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					</header>
					<div class="entry-summary">
		
						<?php the_excerpt(); ?>
		
						<?php if( have_rows('council_files',  get_the_ID() ) ): ?>eeeeeeeee
		
							<ul class='file-list'>
		
								<?php while ( have_rows('council_files' )  ) : the_row();	
							        // display a sub field value
							       $file = get_sub_field('file');
								?>
			
									<li><?php echo $file; ?></li>									
			
							    <?php endwhile; ?>	
		
							</ul>
		
						<?php endif; ?>					
						<div class='view-wrapper'>
							<div class='view dorado-button'><a href="<?php the_permalink(); ?>">View</a></div>
						</div>
					</div>
				</div><!--/end ocol-->
				<div class="clear"></div>
				</div><!--/-->
			</article>
	
			<?php 
				$image_id = get_post_thumbnail_id();
				$image = wp_get_attachment_image_src($image_id,'large');
			?>
	<?php endwhile; ?>
