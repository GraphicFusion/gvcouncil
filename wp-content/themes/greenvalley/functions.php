<?php
/**
 * Sage includes
 *
 * The $dorado_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 */
$dorado_includes = array(
	'lib/utils.php',            			     			// Utility functions
	'lib/init.php',                  						// Initial theme setup and constants
	'lib/wrapper.php',               						// Theme wrapper class
	'lib/conditional-tag-check.php', 						// ConditionalTagCheck class
	'lib/config.php',                						// Configuration
	'lib/enqueue.php',                						// Scripts and stylesheets
	'lib/extras.php',                						// Custom functions
	'lib/dorado_header_hook.php',  							// Header Hook
	'lib/menus.php',  										// Admin Menu functions
	 'lib/nav.php',											// Custom nav modifications
	'lib/admin.php',  										// Admin functions
	'lib/custom-events.php',								// ACF Based Custom Events
	'lib/custom-council-posts.php',							// CPT for Council Content
	'lib/acf-site-settings.php',							// ACF Sitewide Field Definitions and Functions (sidebar etc)
	'lib/dorado_functions.php',								// General Theme Specific Functions 
	'lib/gv-calendar.php',									// General Theme Specific Functions 
	'templates/prefix-image-filter/filter-functions.php',	// Sets up a container for content with a color filter over background and hover state
	'templates/vendor-image-filter/filter-functions.php',	// Sets up a container for content with a color filter over background and hover state
	'templates/events-image-filter/filter-functions.php',	// Sets up a container for content with a color filter over background and hover state
	'templates/first-image-filter/filter-functions.php',	// Sets up a container for content with a color filter over background and hover state
	'templates/second-image-filter/filter-functions.php',	// Sets up a container for content with a color filter over background and hover state
// add extra includes here
);

foreach ($dorado_includes as $file) {
  if (!$filepath = locate_template($file)) {
   // trigger_error(sprintf(__('Error locating %s for inclusion', 'dorado'), $file), E_USER_ERROR);
  }
	else{
  require_once $filepath;
}
}
unset($file, $filepath);

use Sonder\Dorado\Config;   
use Sonder\Dorado\Wrapper;

// ADMIN STYLES
add_action( 'admin_enqueue_scripts', 'dorado_admin_enqueue' );
function dorado_admin_enqueue(){
	wp_register_script( 'dorado_admin_js', get_template_directory_uri() . '/assets/admin/admin-js.js', array('jquery') , null, true );
	wp_enqueue_script('dorado_admin_js');
	wp_register_style( 'dorado_admin_css', get_template_directory_uri() . '/assets/admin/admin-style.css', false, '1.0.0' );
	wp_enqueue_style( 'dorado_admin_css' );
}
