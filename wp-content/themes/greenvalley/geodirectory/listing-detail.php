<div class="container">
	<div class="row">

		<div class="col-lg-9">
			<?php
//				do_action('geodir_top_content', 'details-page');
				
//				do_action('geodir_detail_before_main_content');
				
				do_action('geodir_before_main_content', 'details-page');
								
				do_action('geodir_wrapper_content_open', 'details-page', 'geodir-wrapper-content', '');
				
				do_action('geodir_article_open', 'details-page', 'post-' . get_the_ID(), get_post_class(), 'http://schema.org/LocalBusiness');
				
				if (have_posts() && !$preview) {
				    the_post();
				    global $post, $post_images;
				    /**
				     * Calls the details page main content on the details template page.
				     *
				     * @since 1.1.0
				     * @param object $post The current post object.
				     */
				    do_action('geodir_details_main_content', $post);
				} elseif ($preview) {
				    /**
				     * Called on the details page if the page is being previewed.
				     *
				     * This sets the value of `$post` to the preview values before the main content is called.
				     *
				     * @since 1.1.0
				     */
				    do_action('geodir_action_geodir_set_preview_post'); // set the $post to the preview values
				    /** This action is documented in geodirectory-templates/listing-detail.php */
				    do_action('geodir_details_main_content', $post);
				}
				
				###### MAIN CONTENT WRAPPERS CLOSE ######
				/**
				 * Adds the closing HTML wrapper for the article on the details page.
				 *
				 * @since 1.1.0
				 * @param string $type Page type.
				 * @see 'geodir_article_open'
				 */
				do_action('geodir_article_close', 'details-page');
				
				/** This action is documented in geodirectory-templates/add-listing.php */
				do_action('geodir_after_main_content');
				
				/** This action is documented in geodirectory-templates/add-listing.php */
				do_action('geodir_wrapper_content_close', 'details-page');						
			?>
		</div>
		<div class="col-lg-3">
			<div class="gv-gd-sidebar">
				<div class="vendor-datum">
					<label> <span class="glyphicon glyphicon-phone" aria-hidden="true"></span>Contact</label>
					<span><?php echo $post->geodir_contact; ?></span>
				</div>
				<div class="vendor-datum">
					<label> <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>Location</label>
					<span><?php echo $post->post_address . " , " . $post->post_city; ?></span>
				</div>
				<div class="vendor-datum">
					<label> <span class="glyphicon glyphicon-time" aria-hidden="true"></span>Hours</label>
					<span><?php echo $post->geodir_contact; ?></span>
				</div>
				<div class="vendor-datum">
					<label> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>Website</label>
					<span><?php echo $post->geodir_website; ?></span>
				</div>
				<?php
					$arr = get_post_meta( $post->ID , 'license_expiration'); 
					if( count( $arr ) > 0 ):
						$date = strtotime( $arr[0] );
						$exp = date( 'm/d/Y' , $date ); ?>
					<div class="vendor-datum">
						<label> <span class="glyphicon " aria-hidden="true"></span>License Expiration</label>
						<span><?php echo $exp; ?></span>
					</div>
					<?php endif; ?>
			</div>
		</div>
	</div>
</div>