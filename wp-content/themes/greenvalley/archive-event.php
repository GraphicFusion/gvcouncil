<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
        <?php _e('Sorry, no results were found.', 'dorado'); ?>
    </div>
    <?php get_search_form(); ?>
<?php endif; ?>
	<?php
		$temp = $wp_query;
		$wp_query = null;
		$args = array(
			'post_type' => 'event',
		    'posts_per_page' => 10,
		    'paged' => $paged
		);
		$wp_query = new WP_Query( $args );
		while ($wp_query ->have_posts() ) :	$wp_query->the_post(); 
	?>
			<article <?php post_class(); ?>>
				<div class='row'>
				<div class="col-lg-3">
					<?php the_post_thumbnail('thumbnail'); ?>
				</div>
				<div class="col-lg-9">
					<header>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					</header>
					<div class="entry-summary">
		
						<?php the_excerpt(); ?>
		
						<?php if( have_rows('dorado-events' , $id) ): $i = 0; ?>
		
							<ul class='date-list'>
		
								<?php while ( have_rows('dorado-events', $id)  ) : the_row();	
							        // display a sub field value
							        $date = strtotime( get_sub_field('dorado_event_date') ); // convert date to unix; output by acf as Ymd: 20150618
									if ( $date ){
										$date = date( 'F j, Y' , $date);
									}
							        $time = get_sub_field('dorado_event_time'); 
								?>
			
									<li><?php echo $date; ?><?php if( $time ){ echo " at ". $time; } ?></li>									
			
							    <?php endwhile; ?>	
		
							</ul>
		
						<?php endif; ?>					
						<div class='view-wrapper'>
							<div class='view dorado-button'><a href="<?php the_permalink(); ?>">View</a></div>
						</div>
					</div>
				</div><!--/end ocol-->
				<div class="clear"></div>
				</div><!--/-->
			</article>
	
			<?php 
				$image_id = get_post_thumbnail_id();
				$image = wp_get_attachment_image_src($image_id,'large');
			?>
	<?php endwhile; ?>
<?php if ($wp_query->max_num_pages > 1) : ?>
    <nav class="post-nav">
        <ul class="pager">
			<?php if( $paged > 1 ) : ?>
	            <li class="previous"><a  href='/events/page/<?php echo ($paged - 1); ?>/?'>Previous</a></li>
			<?php endif; ?>
			<?php if( $paged < $wp_query->max_num_pages ) : ?>
	            <li class="next"><a href='/events/page/<?php echo ($paged + 1); ?>/?'>Next</a></li>
			<?php endif; ?>
        </ul>
    </nav>
<?php endif; ?>
