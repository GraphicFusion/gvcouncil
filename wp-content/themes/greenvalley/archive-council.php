<h1>Council Content</h1>
<?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
        <?php _e('Sorry, no results were found.', 'dorado'); ?>
    </div>

    <?php get_search_form(); ?>
<?php endif; ?>
	<?php
		
		while ($wp_query ->have_posts() ) :	$wp_query->the_post(); 
	?>
			<article <?php post_class(); ?>>
				<div class='row file-row'>
				<div class="col-lg-12">
					<header>
						<h3 class="entry-title"><?php the_title(); ?></h3>
					</header>
					<div class="entry-summary">
		
						<?php the_excerpt(); ?>
		
						<?php if( have_rows('council_files' ) ): ?>
		
							<ul class='file-list'>
		
								<?php while ( have_rows('council_files' )  ) : the_row();	
							        // display a sub field value
							       $file = get_sub_field('file');
									$title = " File";
									if( $file['title'] ){
										$title = $file['title'];
									}
								?>
			
									<li><a href="<?php echo $file['url']; ?>" target="_blank">Download <?php echo $title; ?></a></li>									
			
							    <?php endwhile; ?>	
		
							</ul>
		
						<?php endif; ?>					
						<div class='view-wrapper'>
							<div class='view dorado-button'><a href="<?php the_permalink(); ?>">View More</a></div>
						</div>
					</div>
				</div><!--/end ocol-->
				<div class="clear"></div>
				</div><!--/-->
			</article>
	
			<?php 
				$image_id = get_post_thumbnail_id();
				$image = wp_get_attachment_image_src($image_id,'large');
			?>
	<?php endwhile; ?>
